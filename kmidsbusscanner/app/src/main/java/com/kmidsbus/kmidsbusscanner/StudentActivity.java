package com.kmidsbus.kmidsbusscanner;

import android.Manifest;
import android.app.DownloadManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpResponse;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class StudentActivity extends AppCompatActivity {

    private static final String IMAGE_DIRECTORY = "/StudentIMG";
    private String url = "http://kmids.ctt-center.com/api/student";
    private  String HttpUrl = "http://kmids.ctt-center.com/api/school/attendance";
    JSONArray data = null;
    ImageView stdimg;
    private static final int TIME_OUT = 3000;
    private Handler mHandler = new Handler();
    private Context mContext;
    DownloadManager mDManager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.studentactivity);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission (Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission (Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            } else {
                ActivityCompat.requestPermissions (this, new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                }, 999);
            }
        }
        stdimg = (ImageView) findViewById (R.id.imageView2);
        final TextView t1 = (TextView) findViewById (R.id.firstname);
        final TextView l1 = (TextView) findViewById (R.id.lastname);
        final TextView c1 = (TextView) findViewById (R.id.code_student);
        final SharedPreferences getPreferences  = getPreferences (MODE_PRIVATE);
        mDManager = (DownloadManager) getSystemService (DOWNLOAD_SERVICE);
        mContext = getApplicationContext();
        autochangeActivity();
        Uri uri = Uri.parse ("content://db.kmids");
        String cardId=this.getIntent ().getStringExtra ("cardId");
        Cursor cursor = getContentResolver ().query (uri, new String[]{}, "cardId=?", new String[]{cardId}, null);
        while (cursor.moveToNext ()) {
            String studentCode=cursor.getString (1);
            String firstName=cursor.getString (2);
            String lastName=cursor.getString (3);
            File storageDir = getExternalFilesDir (Environment.getExternalStorageState ()+ IMAGE_DIRECTORY);
            //  Toast.makeText (this,cursor.getString (5),Toast.LENGTH_LONG).show ();
            stdimg.setImageURI (Uri.parse ("file://" + storageDir.getAbsolutePath () + "/students/" + cursor.getString (5)));
            t1.setText (Uri.parse(firstName).toString ());
            l1.setText (Uri.parse (lastName).toString ());
            c1.setText (Uri.parse (studentCode).toString ());

            Calendar calendar = Calendar.getInstance ();
            SimpleDateFormat format = new SimpleDateFormat ("HH:mm:ss");
            final String time = format.format (calendar.getTime ());
            calendar = Calendar.getInstance ();
            SimpleDateFormat dateFormat = new SimpleDateFormat ("M/d/yyyy");
            final String date = dateFormat.format (calendar.getTime ());
            final String Finaltime = date + " " + time;

            final String sec = Integer.toString (calendar.get (Calendar.SECOND));
            JSONObject object = null;
            try {
                object = new JSONObject ("{ apiRequest: {action: \"add\"}, " +
                        "student:{studentCode:\"" + studentCode + "\"},attendance:{date:\""
                        + Finaltime + "\", \"location\": \"School Bus\",\"imageURL\": \"schoolbus.jpg\"}}");
            } catch (JSONException e) {
                e.printStackTrace ();
            }
            JsonObjectRequest jsonRequest = new JsonObjectRequest( Request.Method.POST, HttpUrl,object , new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    // Toast.makeText( StudentActivity.this, "success", Toast.LENGTH_SHORT ).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(StudentActivity.this, "error", Toast.LENGTH_SHORT).show();
                }
            } );
            RequestQueue requestQueue = Volley.newRequestQueue( StudentActivity.this );
            requestQueue.add( jsonRequest );
        }

        SharedPreferences.Editor editor = getPreferences.edit ();
        editor.putString ("studentCode", "");
        editor.putLong ("lastUpdate", System.currentTimeMillis () / 1000);
        editor.commit ();
        String lastStudentCode = getPreferences.getString ("studentCode", "");
        Long lastUpdate = getPreferences.getLong ("lastUpdate", 0);
        // Toast.makeText (StudentActivity.this, "" + lastUpdate, Toast.LENGTH_LONG).show ();
        //Toast.makeText (StudentActivity.this, "" + (System.currentTimeMillis () / 1000), Toast.LENGTH_LONG).show ();
        if (!lastStudentCode.equals ("") || lastUpdate == 0 || ((System.currentTimeMillis () / 1000) - lastUpdate > 10)) {
            Toast.makeText (StudentActivity.this, lastStudentCode, Toast.LENGTH_LONG).show ();
        }
    }
    public void autochangeActivity() {
        mHandler.postDelayed (new Runnable () {
            @Override
            public void run() {
                startActivity (new Intent (StudentActivity.this, MainActivity.class));
                finish ();
            }
        }, TIME_OUT);

    }
}
