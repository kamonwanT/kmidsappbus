package com.kmidsbus.kmidsbusscanner;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;

import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;

import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.kmidsbus.kmidsbusscanner.MainActivity.MY_PREFS_NAME;

public class Two extends Activity {
    private static final String IMAGE_DIRECTORY = "/STDIMG";
    DownloadManager mDManager;
    EditText mEtUrl;
    Button mBtnDownload;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.two);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission (Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission (Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            } else {
                ActivityCompat.requestPermissions (this, new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                }, 999);
            }
            final SharedPreferences getPreferences  = getPreferences (MODE_PRIVATE);
            mBtnDownload = (Button) findViewById (R.id.btn_download);
            mEtUrl = (EditText) findViewById (R.id.et_url);
            mDManager = (DownloadManager) getSystemService (DOWNLOAD_SERVICE);
            mBtnDownload.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    String HttpUrl = "http://kmids.next2steps.com/api/student";
                    JSONObject jo = new JSONObject ();
                    JSONObject apiRequest = new JSONObject ();
                    JSONArray data = new JSONArray ();
                    JSONObject busData = new JSONObject ();
                    try {
                        apiRequest.put ("action", "GET");
                        jo.put ("apiRequest", apiRequest);
                        busData.put ("imageName", getPreferences.getString ("name", "None"));
                        //busData.put ("cardId", getPreferences.getString ("NFC", "None"));
                        //busData.put( "date", getDateTime() );
                        data.put (busData);
                        jo.put ("data", data);
                    } catch (JSONException e) {
                        e.printStackTrace ();
                    }
                    JsonObjectRequest jsonRequest = new JsonObjectRequest (Request.Method.POST, HttpUrl, jo, new Response.Listener<JSONObject> () {
                        @Override
                        public void onResponse(JSONObject response) {
                            //Uri uri = Uri.parse ("/IMAGE_DIRECTORY");
                            Uri uri = Uri.parse (mEtUrl.getText().toString ());
                            DownloadManager.Request req = new DownloadManager.Request (uri);
//                req.setTitle ("My File");
//                req.setDescription ("Downloads");
                            req.setNotificationVisibility (DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                            File storageDir = getExternalFilesDir (Environment.DIRECTORY_PICTURES);
                            //Log.d("DirectoryPath",storageDir.getAbsolutePath());
                            req.setDestinationUri (Uri.parse ("file://" + storageDir.getAbsolutePath () + mEtUrl.getText ().toString ()));
                            mDManager.enqueue (req);
//                        Uri uri = Uri.parse (mEtUrl.getText ().toString ());
//                    DownloadManager.Request req = new DownloadManager.Request (uri);
////                req.setTitle ("My File");
////                req.setDescription ("Downloads");
//                    req.setNotificationVisibility (DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//                    File storageDir = getExternalFilesDir (Environment.getExternalStorageState ()+ IMAGE_DIRECTORY);
//                    Log.d("DirectoryPath",storageDir.getAbsolutePath());
//                   req.setDestinationUri (Uri.parse ("file://" + storageDir.getAbsolutePath () + mEtUrl.getText ().toString ()));
//                    mDManager.enqueue (req);

                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            File wallpaperDirectory = new File( Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY );
                            if (!wallpaperDirectory.exists()) {
                                wallpaperDirectory.mkdirs();
                            }
                            try {
                                SharedPreferences settings = getSharedPreferences( MY_PREFS_NAME, MODE_PRIVATE );

                                File f = new File( wallpaperDirectory,  ".jpg" );
                                f.createNewFile();

                                FileOutputStream fo = new FileOutputStream( f );
                                fo.write( bytes.toByteArray() );
                                MediaScannerConnection.scanFile( Two.this, new String[]{f.getPath()}, new String[]{"image/jpeg"}, null );
                                fo.close();
                                Log.d( "TAG", "File Saved::--->" + f.getAbsolutePath() );
                                SharedPreferences.Editor editor = getSharedPreferences( MY_PREFS_NAME, MODE_PRIVATE ).edit();
                                editor.putString( "name", f.getAbsolutePath() );
                                editor.apply();
                                //return f.getAbsolutePath();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            // return "";

//                        Intent i = new Intent( MainActivity.this, TwoActivitys.class );
                            //  startActivity( i );
                        }
                    }, new Response.ErrorListener () {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText (Two.this, "error", Toast.LENGTH_SHORT).show ();
                        }
                    });
                    RequestQueue requestQueue = Volley.newRequestQueue (Two.this);
                    requestQueue.add (jsonRequest);
//                    Uri uri = Uri.parse (mEtUrl.getText().toString ());
//                    DownloadManager.Request req = new DownloadManager.Request (uri);
////                req.setTitle ("My File");
////                req.setDescription ("Downloads");
//                    req.setNotificationVisibility (DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//                    File storageDir = getExternalFilesDir (Environment.DIRECTORY_PICTURES);
//                    //Log.d("DirectoryPath",storageDir.getAbsolutePath());
//                   req.setDestinationUri (Uri.parse ("file://" + storageDir.getAbsolutePath () + mEtUrl.getText ().toString ()));
//                    mDManager.enqueue (req);

                }
            });

        }

    }
}

//        b2.setOnClickListener (new View.OnClickListener () {
//            @Override
//            public void onClick(View v) {
//
//                    DownloadManager downloadmanager = (DownloadManager) getSystemService (Context.DOWNLOAD_SERVICE);
//                    Uri uri = Uri.parse ("http://kmids.ctt-center.com/images/students?");
//                    DownloadManager.Request request = new DownloadManager.Request (uri);
//                    request.setTitle ("My File");
//                    request.setDescription ("Downloads");
//                    request.setNotificationVisibility (DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//                    File storageDir = getExternalFilesDir (Environment.DIRECTORY_PICTURES);
//                    //Log.d("DirectoryPath",storageDir.getAbsolutePath());
//                    request.setDestinationUri (Uri.parse ("file://" + storageDir.getAbsolutePath () + "/"));
//                    downloadmanager.enqueue (request);
//
//
//
//
//
//            }
//
//        });
//    }
//
//    }

