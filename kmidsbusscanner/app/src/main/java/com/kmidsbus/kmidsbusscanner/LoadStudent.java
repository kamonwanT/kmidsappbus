package com.kmidsbus.kmidsbusscanner;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoadStudent extends AppCompatActivity {
    EditText studentCode,firstName,lastName,cardId;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loadstudent);
    }
    public void addStudent(View view)
    {
        ContentValues values = new ContentValues();

        studentCode = (EditText) findViewById (R.id.editText1);
        firstName  = (EditText) findViewById (R.id.editText2);
        lastName  = (EditText) findViewById (R.id.editText3);
        cardId  = (EditText) findViewById (R.id.editText4);

        values.put("studentCode",studentCode.getText ().toString ());
        values.put("firstName",firstName.getText ().toString ());
        values.put("lastName",lastName.getText ().toString ());
        values.put("cardId",cardId.getText ().toString ());
        Uri uri = Uri.parse("content://db.kmids");
        Uri result=getContentResolver().insert(uri,values);
        Toast.makeText(this,"result"+result.toString(),Toast.LENGTH_LONG).show();
    }
    public void getStudent(View view)
    {
        Uri uri = Uri.parse("content://db.kmids");
        Cursor cursor = getContentResolver().query(uri,new String[]{},"cardId=?",new String[]{""},null);
        while(cursor.moveToNext())
        {

           Toast.makeText(this,cursor.getString(0),Toast.LENGTH_LONG).show();


        }
        cursor.close();

    }
}
