package com.kmidsbus.kmidsbusscanner;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

public class KMIDSContentProvider extends ContentProvider {
    public KMIDSContentProvider() {
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = kmidsDb.getWritableDatabase();
        int rows=db.delete("Students",selection,selectionArgs);
        return rows;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    @Override
    public String getType(Uri uri) {
        // TODO: Implement this to handle requests for the MIME type of the data
        // at the given URI.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = kmidsDb.getWritableDatabase();
        long newRowId = db.insert("Students", null, values);
        return ContentUris.withAppendedId(uri,newRowId);
    }

    kmidsDbHelper kmidsDb = null;
    @Override
    public boolean onCreate() {
        kmidsDb  = new kmidsDbHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = kmidsDb.getReadableDatabase();
        Cursor cursor = db.query("Students",projection,selection,selectionArgs,null,null,sortOrder);
        return cursor;
    }


    //SQLITE HELPER

    public class kmidsDbHelper extends SQLiteOpenHelper
    {

        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "kmids.db";

        public kmidsDbHelper(@Nullable Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL("CREATE TABLE Students (_id INTEGER PRIMARY KEY AUTOINCREMENT,studentCode TEXT,firstName TEXT,lastName TEXT,cardId TEXT,imageName TEXT);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS Students");
            onCreate(sqLiteDatabase);
        }

    }

}
