package com.kmidsbus.kmidsbusscanner;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    ActionBar actionBar = null;
    private static final int TIME_OUT = 1000;
    private Handler mHandler = new Handler ();
    private String Null = "null";
    private static final String IMAGE_DIRECTORY = "/StudentIMG";
    String HttpUrl = "http://kmids.ctt-center.com/api/school/attendance";
    private NfcAdapter mAdapter;
    private PendingIntent mPendingIntent;
    private NdefMessage mNdefPushMessage;
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    private static final int REQUEST_CODE = 1;
    private ParseContent parseContent;
    //private Button a;
    private ImageView imageView;
    DownloadManager mDManager;
    ArrayList<HashMap<String, String>> arraylist;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater ().inflate (R.menu.menu_refresh, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId ()) {
            case R.id.menu_refresh:
                loaddata ();
                break;

        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_main);
        final AlertDialog.Builder adb = new AlertDialog.Builder (this);
        StrictMode.setThreadPolicy (new StrictMode.ThreadPolicy.Builder ().permitAll ().build ());
        // a = (Button) findViewById( R.id.button );
        RequestQueue requestQueue;
        parseContent = new ParseContent (this);
        requestQueue = Volley.newRequestQueue (MainActivity.this);
        mDManager = (DownloadManager) getSystemService (DOWNLOAD_SERVICE);
        final SharedPreferences getPreferences = getPreferences (MODE_PRIVATE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission (Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission (Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

            } else {
                ActivityCompat.requestPermissions (this, new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                }, 999);
            }
        }

        getData (getIntent ());
        verifyPermissions ();
        mAdapter = NfcAdapter.getDefaultAdapter (this);
        if (mAdapter == null) {
        }
        mPendingIntent = PendingIntent.getActivity (this, 0, new Intent (this, getClass ()).addFlags (Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
    }

    public  void  loaddata(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("KMIDS Bus Scanner");
        builder.setMessage("อัพเดตข้อมูลนักเรียน")

                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);

                        StringRequest request = new StringRequest(
                                Request.Method.POST,
                                "http://kmids.ctt-center.com/api/student",
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        try {
                                            JSONObject res = new JSONObject (response);
                                            JSONArray data = res.getJSONArray ("data");
                                            Uri uri = Uri.parse ("content://db.kmids");
                                            //File storageDir = getExternalFilesDir (Environment.DIRECTORY_PICTURES);
                                            File storageDir = getExternalFilesDir (Environment.getExternalStorageState ()+ IMAGE_DIRECTORY);
                                            MainActivity.this.getContentResolver ().delete (uri,null,null);
                                            File imageDir = new File(Environment.getExternalStorageState ()+ IMAGE_DIRECTORY + "/students");
                                            String[] children = imageDir.list();
                                            if (children!=null) {
                                                for (int i = 0; i < children.length; i++) {
                                                    new File (imageDir, children[i]).delete ();
                                                }
                                            }
                                            for(int i=0;i<data.length ();i++) {
                                                ContentValues values = new ContentValues ();
                                                values.put ("studentCode",data.getJSONObject (i).getString ("studentCode"));
                                                values.put ("firstName",data.getJSONObject (i).getString ("firstName"));
                                                values.put ("lastName",data.getJSONObject (i).getString ("lastName"));
                                                values.put ("cardId",data.getJSONObject (i).getString ("cardId"));
                                                values.put ("imageName",data.getJSONObject (i).getString ("imageName"));
                                                MainActivity.this.getContentResolver ().insert (uri,values);
                                                String imageName = data.getJSONObject (i).getString ("imageName");
                                                Uri imageUri = Uri.parse ("http://kmids.ctt-center.com/images/students/" + imageName);
                                                DownloadManager.Request req = new DownloadManager.Request (imageUri);
                                                req.setNotificationVisibility (DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                                                Log.d("DirectoryPath",storageDir.getAbsolutePath());
                                                req.setDestinationUri (Uri.parse ("file://" + storageDir.getAbsolutePath () + "/students/" + imageName));
                                                mDManager.enqueue (req);
                                                //Toast.makeText (StudentActivity.this,data.getJSONObject (i).getString ("firstName"),Toast.LENGTH_SHORT).show();
                                            }
                                            // Toast.makeText (MainActivity.this,"Load completed",Toast.LENGTH_LONG).show();

                                        } catch (JSONException e) {
                                            e.printStackTrace ();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                    }
                                })
                        {
                            @Override
                            public byte[] getBody() throws AuthFailureError {
                                String your_string_json = "{\"apiRequest\":{\"action\":\"get_all\"}}"; // put your json
                                return your_string_json.getBytes();
                            }
                            @Override
                            public Map getHeaders() throws AuthFailureError {
                                HashMap headers = new HashMap();
                                headers.put("Content-Type", "application/json");
                                return headers;
                            }
                        };
                        queue.add(request);
                        //MainActivity.this.finish();
                    }
                })
                .setNegativeButton("Cencel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent( intent );
        getData( intent );
    }
    @TargetApi(Build.VERSION_CODES.GINGERBREAD_MR1)
    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    @Override
    protected void onResume() {
        super.onResume();
        if (mAdapter != null) {
            if (mAdapter.isEnabled()) {
                mAdapter.enableForegroundDispatch( this, mPendingIntent, null, null );
                Log.d( "Test", "Must stop here" );
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAdapter != null) {
            mAdapter.disableForegroundDispatch( this );

        }
    }

    private void getData(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals( action )
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals( action )
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals( action )) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra( NfcAdapter.EXTRA_NDEF_MESSAGES );
            NdefMessage[] msgs;
            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                    Intent studentIntent = new Intent (this,StudentActivity.class);
                    studentIntent.putExtra ("cardId",msgs[0].toString());
                    this.startActivity (studentIntent);
                    //  Toast.makeText( this, msgs[i].toString(), Toast.LENGTH_SHORT ).show();
                }
            } else {
                byte[] empty = new byte[0];
                byte[] id = intent.getByteArrayExtra( NfcAdapter.EXTRA_ID );
                Parcelable tag = intent.getParcelableExtra( NfcAdapter.EXTRA_TAG );
                Intent studentIntent = new Intent (this,StudentActivity.class);
                studentIntent.putExtra ("cardId",dumpTagData( tag ));
                this.startActivity (studentIntent);
                Toast.makeText( this, dumpTagData( tag ), Toast.LENGTH_LONG ).show();
                SharedPreferences.Editor editor = getSharedPreferences( MY_PREFS_NAME, MODE_PRIVATE ).edit();
                editor.putString( "NFC", dumpTagData( tag ) );
                editor.apply();
            }
        }
    }

    private String dumpTagData(Parcelable p) {
        StringBuilder sb = new StringBuilder();
        Tag tag = (Tag) p;
        byte[] id = tag.getId();
        sb.append( getDec( id ) );
        return sb.toString();
    }

    private String getHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = bytes.length - 1; i >= 0; --i) {
            int b = bytes[i] & 0xff;
            if (b < 0x10)
                sb.append( '0' );
            sb.append( Integer.toHexString( b ) );
            if (i > 0) {
                sb.append( " " );
            }
        }
        return sb.toString();
    }

    private long getDec(byte[] bytes) {
        long result = 0;
        long factor = 1;
        for (int i = 0; i < bytes.length - 1; ++i) {
            long value = bytes[i] & 0xffl;
            result += value * factor;
            factor *= 256l;
        }
        return result;
    }

    private long getReversed(byte[] bytes) {
        long result = 0;
        long factor = 1;
        for (int i = bytes.length - 1; i >= 0; --i) {
            long value = bytes[i] & 0xffl;
            result += value * factor;
            factor *= 256l;
        }
        return result;
    }

    private void verifyPermissions() {
        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };
        if (ContextCompat.checkSelfPermission( this.getApplicationContext(),
                permissions[0] ) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission( this.getApplicationContext(),
                permissions[1] ) == PackageManager.PERMISSION_GRANTED
        ) {
        } else {
            ActivityCompat.requestPermissions( MainActivity.this, permissions, REQUEST_CODE );
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        verifyPermissions();
    }
}

